package com.company;

class RoboCat extends Pet {
    // constructors
    public RoboCat() {}
    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.species = "Robo Cat";
    }

    // getters
    public String getSpecies() {
        return species;
    }
}