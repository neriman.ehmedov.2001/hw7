package com.company;

class Fish extends Pet {
    // constructors
    public Fish() {}
    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.species = "Fish";
    }

    // getters
    public String getSpecies() {
        return species;
    }
}