package com.company;

class Dog extends Pet {
    // constructors
    public Dog() {}
    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.species = "Dog";
    }

    // methods
    public void foul() {
        System.out.println("I need to cover it up.");
    }

    // getters
    public String getSpecies() {
        return species;
    }
}