package com.company;

public class Main {
    public static void main(String args[]) {
        Man neriman = new Man("Neriman", "Ahmadov", 2001, 160,
                new String[][]{{"Sunday ", "do home work"},
                {"Monday ", "go to courses; watch a film"}, {"Tuesday ", "do workout"},
                {"Wednesday ", "read e-mails"}, {"Thursday ", "do shopping"},
                {"Friday ", "do household"}, {"Saturday ", "visit grandparents"}});

        Woman esma = new Woman("Esma", "Ahmadov", 2002, 160,
                new String[][]{{"Sunday ", "do home work"},
                {"Monday ", "go to courses; watch a film"}, {"Tuesday ", "do workout"},
                {"Wednesday ", "read e-mails"}, {"Thursday ", "do shopping"},
                {"Friday ", "do household"}, {"Saturday ", "visit grandparents"}});
        Man ruslan = new Man("Ruslan", "Hemidov", 2005, 100,
                new String[][]{{"Sunday ", "do  workout"},
                {"Monday ", "go to courses; watch a series"}, {"Tuesday ", "do food"},
                {"Wednesday ", "read novels"}, {"Thursday ", "do shopping"},
                {"Friday ", "go to park and walk"}, {"Saturday ", "visit relatives"}});
        Woman melisa = new Woman("Melisa", "Khurbanova", 1998, 200,
                new String[][]{{"Sunday ", "go to park"},
                        {"Monday ", "do some experiments watch a film"}, {"Tuesday ", "do some cookies"},
                        {"Wednesday ", "read magical spells"}, {"Thursday ", "do online shopping"},
                        {"Friday ", "change some worlds lol"}, {"Saturday ", "visit other planets"}});

        Dog esmasPet = new Dog("Rex", 5, 49, new String[]{"eat, drink, sleep"});
        esma.setPet(esmasPet);
        esma.greetPet();
        esma.describePet();
        esmasPet.respond();
        esmasPet.eat();
        esmasPet.foul();
        System.out.println(esma.toString());
        System.out.println(esmasPet.toString());
        esma.makeUp();

        System.out.println();

        Fish nerimansPet = new Fish("Lucario", 5, 51, new String[]{"eat, drink, sleep"});
        neriman.setPet(nerimansPet);
        neriman.greetPet();
        neriman.describePet();
        nerimansPet.respond();
        nerimansPet.eat();
        System.out.println(neriman.toString());
        System.out.println(nerimansPet.toString());
        neriman.repairCar();

        System.out.println();

        DomesticCat ruslansPet = new DomesticCat("Flareon", 5,
                79, new String[]{"eating, running, drinking"});
        ruslan.setPet(ruslansPet);
        ruslan.greetPet();
        ruslan.describePet();
        ruslansPet.respond();
        ruslansPet.eat();
        ruslan.repairCar();
        System.out.println(ruslan.toString());
        System.out.println(ruslansPet.toString());

        System.out.println();

        RoboCat melisaPet = new RoboCat("Entei", 25,
                80, new String[]{"throw fire balls, hunting, casting spells"});
        melisa.setPet(melisaPet);
        melisa.greetPet();
        melisa.describePet();
        melisaPet.respond();
        melisaPet.eat();
        melisa.makeUp();
        System.out.println(melisa.toString());
        System.out.println(melisaPet.toString());
        melisa.makeUp();
    }
}