package com.company;

final class Woman extends Human {
    // constructors
    public Woman() {}
    public Woman(String name, String surname, int year, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.pet = pet;
    }
    public Woman(String name, String surname, int year, int iq, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
    }
    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    public Woman(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
    }

    // methods
    public void makeUp() {
        System.out.println("It's time to go to the beautician, VUHUU.");
    }
    public void greetPet() {
        System.out.println("Hello, my sweetie " + pet.getNickname() + '!');
    }
}
