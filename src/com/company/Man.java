package com.company;

final class Man extends Human {
    // constructors
    public Man() {}
    public Man(String name, String surname, int year, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.pet = pet;
    }
    public Man(String name, String surname, int year, int iq, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
    }
    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    public Man(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
    }

    // methods
    public void repairCar() {
        System.out.println("It's time to repair car.");
    }
    public void greetPet() {
        System.out.println("Hello, " + pet.getNickname() + '.');
    }
}