package com.company;

import java.util.Arrays;

abstract class Pet {
    // field
    protected String species;
    protected String nickname;
    protected String[] habits;
    protected int age, trickLevel;

    // constructors
    public Pet() {}
    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.species = "UNKNOWN";
    }

    // methods
    public void eat() {
        System.out.println("I am eating.");
    }
    public void respond() {
        System.out.println("Hello, owner. I am - " + nickname + ". I miss you!");
    }

    // Override methods
    @Override
    public String toString() {
        return  species + "{" +
                "nickname = '" + nickname + '\'' +
                ", age = " + age +
                ", trickLevel = " + trickLevel +
                ", habits = " + Arrays.toString(habits) + "}";
    }

    // setters
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setSpecies(String species) {
        this.species = species;
    }

    // getters
    public String getNickname() {
        return nickname;
    }
    public String getHabits() {
        return Arrays.toString(habits);
    }
    public int getAge() {
        return age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }
    public String getSpecies() {
        return species;
    }
}