package com.company;

import java.util.Arrays;

abstract class Human {
    // field
    protected String name, surname;
    protected int year, iq;
    protected String[][] schedule;
    protected Pet pet;

    // constructors
    public Human() {}
    public Human(String name, String surname, int year, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.pet = pet;
    }
    public Human(String name, String surname, int year, int iq, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
    }
    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    public Human(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
    }

    // methods
    public void greetPet() {
        System.out.println("Hello,my Dear  " + pet.getNickname() +"how r u" +'?');
    }
    public void describePet() {
        System.out.print("I have a " + pet.getSpecies() +
                ", he is " + pet.getAge() + " years old, he is ");
        System.out.println((pet.getTrickLevel() > 50) ? "very sly." : "almost not sly.");
    }

    // Override methods
    @Override
    public String toString() {
        return "Human{" +
                "name = '" + name + '\'' +
                ", surname = '" + surname + '\'' +
                ", year = " + year +
                ", iq = " + iq +
                ", schedule = " + Arrays.deepToString(schedule) +"}";
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
    public void setPet(Pet pet) {
        this.pet = pet;
    }

    // getters
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public int getYear() {
        return year;
    }
    public int getIq() {
        return iq;
    }
    public String getSchedule() {
        return Arrays.deepToString(schedule);
    }
    public Pet getPet() {
        return pet;
    }
}