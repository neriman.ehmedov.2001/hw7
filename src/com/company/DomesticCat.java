package com.company;

class DomesticCat extends Pet {
    // constructors
    public DomesticCat() {}
    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.species = "Domestic Cat";
    }

    // methods
    public void foul() {
        System.out.println("I need to cover it up.");
    }

    // getters
    public String getSpecies() {
        return species;
    }
}